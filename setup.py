from setuptools import setup

with open('requirements.txt') as f:
    requirements = f.read().splitlines()


setup(
    name = 'lab_automation',
    version = '0.1',
    packages = ["lab_instruments"],
    python_requires='>=3.8',
    install_requires=requirements,
    # scripts=[]
)
