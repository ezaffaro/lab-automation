# Lab Automation
Python module containing classes to control common laboratory equipment.

## Installation
Open a terminal into the repository and run

```bash
pip3 install --user -e .
```

If you are installing in a virtual environment, you can run the command without the `--user` flag.
