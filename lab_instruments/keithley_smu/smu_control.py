import pyvisa
from ..generic_instrument import GenericInstrumentControl

class SmuControl(GenericInstrumentControl):
  """
  ```
  rm = pyvisa.ResourceManager('@py') # open resource manager
  smu = SmuControl(rm, 'ASRL5::INSTR') # create SmuControl instance
  id = smu.inst.query('*IDN?') # standard VISA query, id contains the scope ID (+ a newline character)
  ```
  """
  def __init__(self, resource_manager: pyvisa.ResourceManager, resource_name: str, timeout=10000):
    super().__init__(resource_manager, resource_name, timeout=timeout)


  def __del__(self):
    if self.model == 'MODEL 2400':
      self.write('SYST:LOC')
    return super().__del__()


  def reset(self):
    self.write('*RST')
    self.write('*CLS')
  

  def output_on(self, on: bool):
    self.write(f'OUTP {"ON" if on else "OFF"}')
  

  def read_volt_curr(self):
    if self.model == 'MODEL 2400':
    # ask the SMU to output voltage and current values
      self.write('FORM:ELEM VOLT,CURR')
      # read and parse the values
      val = self.query_ascii_values('READ?')
      return tuple(val)
    elif self.model == 'MODEL 2450':
      self.write('SENS:FUNC "VOLT"')
      v = self.query_ascii_values('READ?')[0]
      self.write('SENS:FUNC "CURR"')
      i = self.query_ascii_values('READ?')[0]
      return v, i
  

  def set_source_voltage_range(self, range: float):
    if range == 'auto':
      self.write('SOUR:VOLT:RANG:AUTO 1')
    else:
      self.write(f'SOUR:VOLT:RANG {range}')
  

  # def set_source_current_limit(self, limit: float):
  #   self.write(f'SOUR:CURR:LIM {limit}')


  def set_sense_current_range(self, range: float):
    if range == 'auto':
      self.write('SENS:CURR:RANG:AUTO 1')
    else:
      self.write(f'SENS:CURR:RANG {range}')
  

  def set_voltage(self, voltage: float):
    self.write(f'SOUR:VOLT:LEV:IMM:AMPL {voltage}')
  

  def set_averaging(self, counts: int):
    if counts <= 0:
      self.write('SENS:AVER OFF')
    else:
      self.write('SENS:AVER:TCON REP')
      self.write(f'SENS:AVER:COUN {counts}')
      self.write('SENS:AVER ON')

  
  

