import pyvisa


class GenericInstrumentControl:

  def __init__(self, resource_manager: pyvisa.ResourceManager, resource_name: str, timeout=10000):
    """
    Opens and resets the device.

    Parameters:
    resource_manager -- the pyVISA resource manager, see pyVISA docs https://pyvisa.readthedocs.io/en/latest/
    resource_name -- name of the resource, like 'ASRL5::INSTR' if over RS232
    timeout -- timeout when waiting for a response from the instrument, in ms
    """
    self.rm = resource_manager

    # open the smu resource. 
    self.inst = self.rm.open_resource(resource_name)
    # set up the resource if using RS232
    if 'ASRL' in resource_name.upper():
      self.inst.write_termination='\r'
      self.inst.read_termination='\r'
      self.inst.baud_rate = 9600
      self.inst.data_bits = 8
      self.inst.parity = pyvisa.constants.Parity.none
      self.inst.flow_control = pyvisa.constants.VI_ASRL_FLOW_NONE

    # timeout in ms
    self.inst.timeout = timeout

    _id = self.query('*IDN?').split(',')
    self.manufacturer = _id[0]
    self.model = _id[1]
    self.serial_number = _id[2]
  

  def __del__(self):
    # close the instrument in the destructor
    self.inst.close()
  

  def _check_error(self, command):
    err = self.inst.query('SYST:ERR?')
    errno, errmsg = err.split(',')
    errno = int(errno)
    if errno:
      raise RuntimeError(f'Error {errno} using \'{command}\': {errmsg}')


  def write(self, *args, **kwargs):
    self.inst.write(*args, **kwargs)
    # self._check_error(args[0])
  

  def query(self, *args, **kwargs):
    ret = self.inst.query( *args, **kwargs)
    # self._check_error(args[0])
    return ret
  

  def query_binary_values(self, *args, **kwargs):
    ret = self.inst.query_binary_values( *args, **kwargs)
    # self._check_error(args[0])
    return ret
  

  def query_ascii_values(self, *args, **kwargs):
    ret = self.inst.query_ascii_values( *args, **kwargs)
    # self._check_error(args[0])
    return ret
  
