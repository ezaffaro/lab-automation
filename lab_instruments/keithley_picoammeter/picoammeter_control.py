import pyvisa
from ..generic_instrument import GenericInstrumentControl

class PicoammeterControl(GenericInstrumentControl):
  """
  ```
  rm = pyvisa.ResourceManager('@py') # open resource manager
  smu = PicoammeterControl(rm, 'ASRL5::INSTR') # create PicoammeterControl instance
  id = smu.inst.query('*IDN?') # standard VISA query, id contains the scope ID (+ a newline character)
  ```
  """
  def __init__(self, resource_manager: pyvisa.ResourceManager, resource_name: str, timeout=10000):
    """
    Opens the picoammeter device.

    Parameters:
    resource_manager -- the pyVISA resource manager, see pyVISA docs https://pyvisa.readthedocs.io/en/latest/
    resource_name -- name of the resource, like 'ASRL5::INSTR' if over RS232
    timeout -- timeout when waiting for a response from the scope, in ms
    """
    super().__init__(resource_manager, resource_name, timeout=timeout)


  def reset(self):
    self.write('*RST')
    self.write('*CLS')
  

  def set_averaging(self, counts: int):
    if counts <= 0:
      self.write('AVER OFF')
    else:
      self.write('AVER:COUN {}'.format(counts))
      self.write('AVER:TCON REP')
      self.write('AVER ON')
  

  def perform_zero_correction(self, range: float = 50e-9):
    self.write('SYST:ZCH ON') # enable zero check
    self.write(f'RANG {range}') # set range
    self.write('INIT') # trigger measurememnt to be used as zero correction
    self.write('SYST:ZCOR:ACQ') # use last reading as zero correction
    self.write('SYST:ZCOR ON') # perform zero correction
    self.write('RANG:AUTO ON') # set auto range
    self.write('SYST:ZCH OFF') # disable zero check
  

  def measure_current(self):
    self.write('FORM:ELEM READ')
    return self.query_ascii_values('READ?')[0]