import struct
import numpy as np
from datetime import datetime

class LecroyTrc:
  """
  Class that represents a Lecroy TRC file, containing all its information.
  The parsing relies heavily on struct.

  The memory usage is roughly ~ 10x the trc file size because of the use of 64-bit floats.
  """

  _map_timebase = ['1 ps/div', '2 ps/div', '5 ps/div', '10 ps/div', '20 ps/div', '50 ps/div', '100 ps/div', '200 ps/div', '500 ps/div', '1 ns/div', '2 ns/div', '5 ns/div', '10 ns/div', '20 ns/div', '50 ns/div', '100 ns/div', '200 ns/div', '500 ns/div', '1 us/div', '2 us/div', '5 us/div', '10 us/div', '20 us/div', '50 us/div', '100 us/div', '200 us/div', '500 us/div', '1 ms/div', '2 ms/div', '5 ms/div', '10 ms/div', '20 ms/div', '50 ms/div', '100 ms/div', '200 ms/div', '500 ms/div', '1 s/div', '2 s/div', '5 s/div', '10 s/div', '20 s/div', '50 s/div', '100 s/div', '200 s/div', '500 s/div', '1 ks/div', '2 ks/div', '5 ks/div']

  _map_fixed_vert_gain = ['1 uV/div', '2 uV/div', '5 uV/div', '10 uV/div', '20 uV/div', '50 uV/div', '100 uV/div', '200 uV/div', '500 uV/div', '1 mV/div', '2 mV/div', '5 mV/div', '10 mV/div', '20 mV/div', '50 mV/div', '100 mV/div', '200 mV/div', '500 mV/div', '1 V/div', '2 V/div', '5 V/div', '10 V/div', '20 V/div', '50 V/div', '100 V/div', '200 V/div', '500 V/div', '1 kV/div']

  _map_wave_source = {0: 'CHANNEL 1', 1: 'CHANNEL 2', 2: 'CHANNEL 3', 3: 'CHANNEL 4', 9: 'UNKNOWN'}

  _map_coupling = ['DC 50 Ohm', 'GND', 'DC 1MOhm', 'GND', 'AC 1 MOhm']

  _map_processing = ['no processing', 'fir filter', 'interpolated', 'sparsed', 'autoscaled', 'no result', 'rolling', 'cumulative']

  _map_record_type = ['single sweep', 'interleaved', 'histogram', 'graph', 'filter coefficient', 'complex', 'extrema','sequence obsolete', 'centered RIS', 'peak detect']

  def __init__(self, trc):
    """
    Parses a `bytes` object and store its content as data members.

    If reading from a file:
    ```
    with open('file.trc'), 'rb') as f:
      trc_bytes = f.read()

    trc = LecroyTrc(trc_bytes)
    print(trc.x) # prints the timebase
    print(trc.y) # prints the waveforms array

    If reading from the scope directly:
    osc = LecroyVbs(rm, 'TCPIP0::10.195.49.11::INSTR')
    ...
    # setup oscilloscope
    ...

    trc_bytes = osc.inst.query_binary_values('C1:WAVEFORM?'.format(i), datatype='s')[0] # this is equivalent to the content of a TRC file
    trc = LecroyTrc(trc_bytes)
    print(trc.x) # prints the timebase
    print(trc.y) # prints the waveforms array
    ```

    See the code for all the available data members (everything of the form self.xxx is available as trc.xxx)
    """
    # based on https://github.com/freespace/pylecroy/blob/master/lecroy.py
    # and https://github.com/bennomeier/leCroyParser/blob/master/lecroyparser/__init__.py
    beginning, = struct.unpack('50s', trc[:50])
    offset = beginning.find(b'WAVEDESC')

    _, template, fmt, endianness = struct.unpack('16s16s2H', trc[offset:offset+36])
    #print(template, fmt, endianness)

    if fmt == 0:
      data_type = 'i1' # 8-bit data
    else:
      data_type = 'i2' # 16-bit data

    if endianness == 0:
      end_chr = '>' # big endian
    else:
      end_chr = '<' # little endian

    l_wave_descriptor, l_user_text, l_res_desc1_size, l_trig_time_array, l_ris_time_array, l_array1, l_wave_array_1, _, _, _ = struct.unpack(end_chr+'10i', trc[offset+36:offset+76]) # what are the others?
    #print(l_wave_descriptor, l_user_text, l_res_desc1_size, l_trig_time_array, l_ris_time_array, l_array1, l_wave_array_1)

    self.inst_name, self.inst_number, self.trc_label = struct.unpack('16sl16s', trc[offset+76:offset+116])

    w_array_count, points_per_screen, first_valid_point, last_valid_point, first_point, sparsing_factor, segment_index, segment_count, sweeps_per_acq = struct.unpack(end_chr+'9I', trc[offset+116:offset+152])
    #print(w_array_count, points_per_screen, first_valid_point, last_valid_point, first_point, sparsing_factor, segment_index, segment_count, sweeps_per_acq)

    points_per_pair, pair_offset = struct.unpack(end_chr+'2h', trc[offset+152:offset+156])
    #print(points_per_pair, pair_offset) # what are these???
    
    self.vertical_gain, self.vertical_offset, max_value, min_value = struct.unpack(end_chr+'4f', trc[offset+156:offset+172])
    #print(vertical_gain, vertical_offset, max_value, min_value)
    
    nominal_bits, nom_subarray_count = struct.unpack(end_chr+'2h', trc[offset+172:offset+176])
    #print(nominal_bits, nom_subarray_count) # bit ADC? n segments?

    self.horizontal_interval, self.horizontal_offset, pixel_offset = struct.unpack(end_chr+'f2d', trc[offset+176:offset+196])
    #print(horiz_interval, horiz_offset, pixel_offset) # diff between horizontal offest and pixel offset??
    
    vert_unit, hor_unit = struct.unpack('48s48s', trc[offset+196:offset+292])
    #print(vert_unit, hor_unit)
    
    horizontal_uncertainty, = struct.unpack(end_chr+'f', trc[offset+292:offset+296])
    #print(hor_uncertainty)

    sec, mins, hour, day, month, year = struct.unpack(end_chr+'d4BI', trc[offset+296:offset+312])
    #print(sec, mins, hour, day, month, year)
    #timestamp as a datetime type
    self.timestamp = datetime(year, month, day, hour, mins, int(sec), int((sec-int(sec))*1e6)) # you need to pass seconds and microsecons (int)
    #print(self.timestamp)

    self.acquisition_duration, = struct.unpack(end_chr+'f', trc[offset+312:offset+316])
    
    record_type, processing_done = struct.unpack(end_chr+'2H', trc[offset+316:offset+320])
    self.record_type = self._map_record_type[record_type]
    self.processing_done = self._map_processing[processing_done]
    
    #print('unknown', struct.unpack('2B', trc[320:322])) #???
    
    ris_sweeps, timebase, vert_coupling = struct.unpack(end_chr+'3H', trc[offset+322:offset+328])
    #print(ris_sweeps)
    if timebase == 1000:
      self.timebase = 'external'
    else:
      self.timebase = self._map_timebase[timebase]
    self.coupling = self._map_coupling[vert_coupling]

    self.probe_att, = struct.unpack(end_chr+'f', trc[offset+328:offset+332])

    fixed_vert_gain, bwl = struct.unpack(end_chr+'2H', trc[offset+332:offset+336])
    #print(fixed_vert_gain, bwl)
    self.fixed_vert_gain = self._map_fixed_vert_gain[fixed_vert_gain]
    self.bwl = bool(bwl)

    self.vertical_vernier, self.acq_vert_offset = struct.unpack(end_chr+'2f', trc[offset+336:offset+344])

    wave_source, = struct.unpack(end_chr+'H', trc[offset+344:offset+346])
    #print(self._map_wave_source[wave_source])
    self.wave_source = self._map_wave_source[wave_source]
    
    trigger_timestamps = np.frombuffer(trc[offset+346:], dtype='d', count=int(l_trig_time_array/8))
    self.trigger_timestamps = trigger_timestamps[::2] # timestamps af all the segments in seconds (first is 0.0)

    #TODO need to skip the text?

    # the data is stored in numpy arrays
    self.y_raw = np.frombuffer(trc[offset+346:], dtype=data_type, count=l_wave_array_1, offset=l_trig_time_array) # waveforms in DAC counts

    self.y_raw = np.reshape(self.y_raw, (segment_count, -1)).T
    self.y = self.y_raw * self.vertical_gain - self.vertical_offset # waveforms in V
    
    self.x_raw = np.arange(self.y.shape[0])
    self.x = self.x_raw * self.horizontal_interval + self.horizontal_offset # horizontal scale in seconds
