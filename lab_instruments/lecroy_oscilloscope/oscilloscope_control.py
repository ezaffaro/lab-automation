import pyvisa
from ..generic_instrument import GenericInstrumentControl


class OscilloscopeControl(GenericInstrumentControl):
  """
  Class used to remotely control the oscilloscope.
  After initialization, VBS commands can be sent with write_vbs and query_vbs
  The list of available commands can be checked on the scope itself, minimizing the
  waveform software, opening XStreamBrowser and connecting to the scope.

  Standard VISA commands can be sent as well, using the inst member.

  More details can be found here: https://teledynelecroy.com/doc/tutorial-visual-basic-scripting
  
  Example:
  ```
  rm = pyvisa.ResourceManager('@py') # open resource manager
  osc = OscilloscopeControl(rm, 'TCPIP0::10.195.49.11::INSTR') # create OscilloscopeControl instance
  id = osc.inst.query('*IDN?') # standard VISA query, id contains the scope ID (+ a newline character)
  osc.write_vbs('app.Acquisition.TriggerMode = "Single"') # write VBS command
  triggermode = osc.query_vbs('app.Acquisition.TriggerMode') # triggermode should be equal to "Single"
  ```
  """
  def __init__(self, resource_manager: pyvisa.ResourceManager, resource_name: str, timeout=100000):
    """
    Opens the oscilloscope device.

    Parameters:
    resource_manager -- the pyVISA resource manager, see pyVISA docs https://pyvisa.readthedocs.io/en/latest/
    resource_name -- name of the resource, like 'TCPIP0::<IP ADDRESS>::INSTR' if over ethernet
    timeout -- timeout when waiting for a response from the scope, in ms
    """
    super().__init__(resource_manager, resource_name, timeout=timeout)

    # resets the device
    self.inst.clear()
    # this prevent the instrument from repeating the command when it sends data
    self.write("COMM_HEADER OFF")


  def _check_error(self, command):
    # TODO implement error checking
    pass

  
  def write_vbs(self, command):
    """
    Write a VBS (visual basic [?]) command
    """
    message = 'VBS \'' + command + '\''
    return self.write(message)

  def query_vbs(self, command):
    """
    Query a VBS command
    """
    message = 'VBS? \'Return=' + command + '\''
    return self.query(message)[:-1]


  def read_data(self, channels=[1, 2, 3, 4]):
    raw_data = {}
    for i in channels:
      while True:
        try:
          raw_data[i] = bytes(self.query_binary_values(f'C{i}:WAVEFORM?', datatype='s'))
          break
        except pyvisa.errors.VisaIOError:
          print('WARNING: could not read channel {}. Retrying...'.format(i))
          pass
    
    return raw_data

