#!/usr/bin/env python3
import pyvisa
from lab_instruments.lecroy_oscilloscope import OscilloscopeControl
from lab_instruments.lecroy_oscilloscope import LecroyTrc

def main():
  rm = pyvisa.ResourceManager('@py') # open resource manager
  osc = OscilloscopeControl(rm, 'TCPIP0::128.178.67.110::INSTR') # create OscilloscopeControl instance
  oscid = osc.inst.query('*IDN?') # standard VISA query, id contains the scope ID (+ a newline character)
  print(oscid)
  osc.write_vbs('app.Acquisition.TriggerMode = "Single"') # write VBS command
  triggermode = osc.query_vbs('app.Acquisition.TriggerMode') # triggermode should be equal to "Single"
  print(triggermode)
  raw_data = osc.read_data(channels=[1])

  for d in raw_data.values():
    data = LecroyTrc(d)
    print(data.x)
    print(data.y)
    print(data.timebase)

if __name__ == '__main__':
  main()