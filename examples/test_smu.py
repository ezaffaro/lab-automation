#!/usr/bin/env python3
import pyvisa
import time
from lab_instruments.keithley_smu import SmuControl

def main():
  rm = pyvisa.ResourceManager('@py') # open resource manager
  print(rm.list_resources())
  smu = SmuControl(rm, 'TCPIP0::128.178.67.124::INSTR')
  # smu = SmuControl(rm, 'USB0::1510::9296::04500700::0::INSTR')
  # smu = SmuControl(rm, 'ASRL/dev/ttyUSB0::INSTR')
  smu.reset()
  print(smu.model)

  smu.set_source_voltage_range('auto')
  smu.set_sense_current_range(1e-6)
  # smu.set_source_current_limit(1e-5)

  smu.set_voltage(12)

  smu.output_on(True)
  smu.set_averaging(10)
  print(smu.read_volt_curr())
  smu.set_averaging(0)


if __name__ == '__main__':
  main()