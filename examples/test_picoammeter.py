#!/usr/bin/env python3
import pyvisa
import time
from lab_instruments.keithley_picoammeter import PicoammeterControl

def main():
  rm = pyvisa.ResourceManager('@py') # open resource manager
  print(rm.list_resources())
  # smu = SmuControl(rm, 'TCPIP0::128.178.67.124::INSTR')
  # smu = SmuControl(rm, 'USB0::1510::9296::04500700::0::INSTR')
  picoamm = PicoammeterControl(rm, 'ASRL/dev/ttyUSB0::INSTR', timeout=100000)
  picoamm.reset()

  picoamm.set_averaging(0)

  print('zero check')
  picoamm.perform_zero_correction()

  print('measure')
  print(picoamm.measure_current())  
  

if __name__ == '__main__':
  main()