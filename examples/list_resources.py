#!/usr/bin/env python3
import pyvisa
from lab_instruments.generic_instrument import GenericInstrumentControl 

def main():
  rm = pyvisa.ResourceManager('@py') # open resource manager
  rs_list = rm.list_resources()

  for rs in rs_list:
    try:
      gi = GenericInstrumentControl(rm, rs)
      print(rs, gi.manufacturer, gi.model)
    except Exception:
      print(rs, '***')
    # print(rs)
    # try:
    #   inst = rm.open_resource(rs)
    #   inst.timeout = 10000
    #   print(rs, inst.query('*IDN?'))
    #   inst.close()
    # except Exception as e:
    #   print(rs, '***')

if __name__ == '__main__':
  main()