#!/usr/bin/env python3
import pyvisa
from lab_instruments.generic_instrument import GenericInstrumentControl

def main():
  rm = pyvisa.ResourceManager('@py') # open resource manager
  print(rm.list_resources())
  inst = GenericInstrumentControl(rm, 'TCPIP0::128.178.67.124::INSTR')
  print(inst.manufacturer)
  print(inst.model)
  print(inst.query('*ESE?'))
  print(inst.query('*ESR?'))
  inst.write('*OPC')
  print(inst.query('*ESR?'))
  print(inst.query('*ESR?'))
  
if __name__ == '__main__':
  main()